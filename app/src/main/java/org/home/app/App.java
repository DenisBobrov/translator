package org.home.app;

import org.home.translator.interpreter.brainfuck.analizer.BrainfuckLexicalAnalyzer;
import org.home.translator.interpreter.brainfuck.command.BrainfuckCommandExecutor;
import org.home.translator.interpreter.brainfuck.interpreter.BrainfuckInterpreter;
import org.home.translator.interpreter.brainfuck.interpreter.BrainfuckProgram;

import java.io.*;
import java.nio.file.Paths;

/**
 * Hello world!
 *
 */
public class App 
{
    private static final String FILENAME = "program.bf";

    public static void main( String[] args )
    {
        String code = getCodeFromFile(FILENAME);

        BrainfuckProgram program = new BrainfuckProgram(
                new BufferedReader(new InputStreamReader(System.in)),
                new PrintWriter(System.out)
        );

        new BrainfuckInterpreter().interpret(new BrainfuckLexicalAnalyzer(code), new BrainfuckCommandExecutor(program));
    }

    private static String getCodeFromFile(final String filename) {
        StringBuilder content = new StringBuilder();
        FileReader fileReader = null;

        try {
            fileReader = new FileReader(filename);

            int in;

            while ((in = fileReader.read()) != -1) {
                content.append((char) in);
            }
        } catch (FileNotFoundException fnfe) {
            Paths.get("");
            System.out.println(String.format(
                    "File '%s' not found. Please create it at current location (%s).",
                    filename,
                    Paths.get("").toAbsolutePath()
            ));
        } catch (IOException ioe) {
            // handle exception
        } finally {
            if (fileReader != null) {
                try {
                    fileReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return content.toString();
    }
}
