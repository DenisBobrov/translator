package org.home.translators.core.token;

public interface Token<T extends Enum> {
    public String getValue();
    public int getStartPos();
    public int getEndPos();
    public T getTokenType();
}
