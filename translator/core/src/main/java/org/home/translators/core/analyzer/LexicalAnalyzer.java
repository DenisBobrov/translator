package org.home.translators.core.analyzer;

import org.home.translators.core.token.Token;

public interface LexicalAnalyzer {
    public Token nextToken();
    public boolean hasNextToken();
}
