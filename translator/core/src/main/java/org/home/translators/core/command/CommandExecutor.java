package org.home.translators.core.command;

import org.home.translators.core.token.Token;

public interface CommandExecutor {
    public void executeCommand(Token token);
}
