package org.home.translators.core.interpreter;

import org.home.translators.core.analyzer.LexicalAnalyzer;
import org.home.translators.core.command.CommandExecutor;

public interface Interpretable {
    public void interpret(LexicalAnalyzer analyzer, CommandExecutor executor);
}
