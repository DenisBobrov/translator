package org.home.translators.core.command;

public interface Command {
    public void execute();
}
