package org.home.translator.interpreter.brainfuck.analizer;

import org.home.translator.interpreter.brainfuck.token.BrainfuckTokenType;
import org.home.translators.core.analyzer.LexicalAnalyzer;
import org.home.translators.core.token.Token;

import java.util.NoSuchElementException;

public class BrainfuckLexicalAnalyzer implements LexicalAnalyzer {

    private String code;
    private int currentPosition;

    public BrainfuckLexicalAnalyzer(String code) {
        this.code = code;
        currentPosition = 0;
    }

    @Override
    public boolean hasNextToken() {
        boolean hasNextToken = currentPosition < code.length();

        if (hasNextToken) {
            char in = code.charAt(currentPosition);

            if (Character.isSpaceChar(in) || Character.isISOControl(in)) {
                currentPosition++;

                return hasNextToken();
            }
        }

        return hasNextToken;
    }

    @Override
    public Token nextToken() {
        if (!hasNextToken()) {
            throw new NoSuchElementException();
        }

        char in = code.charAt(currentPosition);

        currentPosition++;

        return createToken(in);
    }

    private Token createToken(final char in) {
        final BrainfuckTokenType tokenType;
        final String tokenValue;

        switch (in) {
            case '+':
                tokenType = BrainfuckTokenType.Increment;
                tokenValue = String.valueOf(in);
                break;
            case '-':
                tokenType = BrainfuckTokenType.Decrement;
                tokenValue = String.valueOf(in);
                break;
            case '>':
                tokenType = BrainfuckTokenType.Next;
                tokenValue = String.valueOf(in);
                break;
            case '<':
                tokenType = BrainfuckTokenType.Prev;
                tokenValue = String.valueOf(in);
                break;
            case '.':
                tokenType = BrainfuckTokenType.Print;
                tokenValue = String.valueOf(in);
                break;
            case ',':
                tokenType = BrainfuckTokenType.Read;
                tokenValue = String.valueOf(in);
                break;
            case '[':
                tokenType = BrainfuckTokenType.While;
                tokenValue = getWhileTokenValue();
                break;
            case ']':
                throw new IllegalStateException("Start loop statement '[' is omitted");
            default:
                throw new IllegalStateException(String.format("Could not recognize token '%s'", in));
        }

        return new Token() {
            @Override
            public String getValue() {
                return tokenValue;
            }

            @Override
            public int getStartPos() {
                return currentPosition;
            }

            @Override
            public int getEndPos() {
                return currentPosition + tokenValue.length();
            }

            @Override
            public Enum getTokenType() {
                return tokenType;
            }

            @Override
            public String toString() {
                return String.format("Token: '%s', type: %s", this.getValue(), getTokenType());
            }
        };
    }

    private String getWhileTokenValue() {
        int rightSquareBracketCount = 1;

        StringBuilder whileTokenValue = new StringBuilder();

        while (true) {
            if (!hasNextToken()) {
                break;
            }

            char in = code.charAt(currentPosition);

            if (in == '[') {
                rightSquareBracketCount++;
            } else if (in == ']') {
                rightSquareBracketCount--;
            }

            currentPosition++;

            if (rightSquareBracketCount == 0) {
                break;
            }

            whileTokenValue.append(in);
        }

        if (rightSquareBracketCount != 0) {
            throw new IllegalStateException("End loop statement ']' is ommited.");
        }

        return whileTokenValue.toString();
    }
}
