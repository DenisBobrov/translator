package org.home.translator.interpreter.brainfuck.interpreter;

import org.home.translators.core.analyzer.LexicalAnalyzer;
import org.home.translators.core.command.CommandExecutor;
import org.home.translators.core.interpreter.Interpretable;
import org.home.translators.core.token.Token;

public class BrainfuckInterpreter implements Interpretable {

    @Override
    public void interpret(LexicalAnalyzer analyzer, CommandExecutor executor) {
        while (analyzer.hasNextToken()) {
            Token token = analyzer.nextToken();

            executor.executeCommand(token);
        }
    }
}
