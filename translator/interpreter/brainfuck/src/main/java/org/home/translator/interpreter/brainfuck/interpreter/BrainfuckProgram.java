package org.home.translator.interpreter.brainfuck.interpreter;

import java.io.*;
import java.util.Scanner;

public class BrainfuckProgram {
    private static int SIZE = 30000;

    private int i;
    private char[] arr;
    private Reader reader;
    private Writer writer;

    public BrainfuckProgram(Reader reader, Writer writer) {
        i = 0;
        arr = new char[SIZE];

        this.reader = reader;
        this.writer = writer;
    }

    public void moveToNextCeil() {
        i++;
    }

    public void moveToPrevCeil() {
        i--;
    }

    public void incrementCeil() {
        arr[i]++;
    }

    public void decrementCeil() {
        arr[i]--;
    }

    public char getCeilValue() {
        return arr[i];
    }

    public void printCeil() {
        try {
            writer.write(String.valueOf(arr[i]));
            writer.flush();

        } catch (IOException ex) {
            // handle exception
        }
    }

    public void readCeil() {
        try {
            writer.write("Please enter a char: ");
            writer.flush();

            Scanner scanner = new Scanner(reader);

            arr[i] = scanner.next().charAt(0);
        } catch (IOException e) {
            // handle exception
        }
    }
}
