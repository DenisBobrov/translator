package org.home.translator.interpreter.brainfuck.command;

import org.home.translator.interpreter.brainfuck.interpreter.BrainfuckProgram;
import org.home.translators.core.command.Command;

abstract public class AbstractCommand implements Command {

    protected BrainfuckProgram program;

    public AbstractCommand(BrainfuckProgram program) {
        this.program = program;
    }
}
