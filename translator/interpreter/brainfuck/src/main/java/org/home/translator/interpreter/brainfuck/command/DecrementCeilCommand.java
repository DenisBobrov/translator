package org.home.translator.interpreter.brainfuck.command;

import org.home.translator.interpreter.brainfuck.interpreter.BrainfuckProgram;

public class DecrementCeilCommand extends AbstractCommand {

    public DecrementCeilCommand(BrainfuckProgram program) {
        super(program);
    }

    @Override
    public void execute() {
        program.decrementCeil();
    }
}
