package org.home.translator.interpreter.brainfuck.command;

import org.home.translator.interpreter.brainfuck.analizer.BrainfuckLexicalAnalyzer;
import org.home.translator.interpreter.brainfuck.interpreter.BrainfuckInterpreter;
import org.home.translator.interpreter.brainfuck.interpreter.BrainfuckProgram;

public class WhileCommand extends AbstractCommand {

    private String code;

    public WhileCommand(BrainfuckProgram program, String code) {
        super(program);

        this.code = code;
    }

    @Override
    public void execute() {
        BrainfuckInterpreter interpreter = new BrainfuckInterpreter();

        while (program.getCeilValue() != 0) {
            interpreter.interpret(new BrainfuckLexicalAnalyzer(code), new BrainfuckCommandExecutor(program));
        }
    }
}
