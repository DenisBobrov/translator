package org.home.translator.interpreter.brainfuck.command;

import org.home.translator.interpreter.brainfuck.interpreter.BrainfuckProgram;

public class IncrementCeilCommand extends AbstractCommand {

    public IncrementCeilCommand(BrainfuckProgram program) {
        super(program);
    }

    @Override
    public void execute() {
        program.incrementCeil();
    }
}
