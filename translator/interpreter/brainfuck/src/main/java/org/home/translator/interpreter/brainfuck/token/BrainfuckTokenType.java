package org.home.translator.interpreter.brainfuck.token;

public enum BrainfuckTokenType {
    Next,
    Prev,
    Increment,
    Decrement,
    Read,
    Print,
    While
}
