package org.home.translator.interpreter.brainfuck.command;

import org.home.translator.interpreter.brainfuck.interpreter.BrainfuckProgram;
import org.home.translator.interpreter.brainfuck.token.BrainfuckTokenType;
import org.home.translators.core.command.CommandExecutor;
import org.home.translators.core.token.Token;

public class BrainfuckCommandExecutor implements CommandExecutor {

    private BrainfuckProgram program;

    public BrainfuckCommandExecutor(BrainfuckProgram program) {
        this.program = program;
    }

    @Override
    public void executeCommand(Token token) {
        BrainfuckTokenType tokenType = (BrainfuckTokenType) token.getTokenType();

        switch (tokenType) {
            case Next:
                new NextCeilCommand(program).execute();
                break;
            case Prev:
                new PrevCeilCommand(program).execute();
                break;
            case Increment:
                new IncrementCeilCommand(program).execute();
                break;
            case Decrement:
                new DecrementCeilCommand(program).execute();
                break;
            case Print:
                new PrintCeilCommand(program).execute();
                break;
            case Read:
                new ReadCommand(program).execute();
                break;
            case While:
                new WhileCommand(program, token.getValue()).execute();
                break;
            default:
                throw new IllegalStateException("Could not execute command");
        }
    }
}
