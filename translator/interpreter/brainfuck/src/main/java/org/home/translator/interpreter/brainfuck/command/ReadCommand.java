package org.home.translator.interpreter.brainfuck.command;

import org.home.translator.interpreter.brainfuck.interpreter.BrainfuckProgram;

public class ReadCommand extends AbstractCommand {

    public ReadCommand(BrainfuckProgram program) {
        super(program);
    }

    @Override
    public void execute() {
        program.readCeil();
    }
}
