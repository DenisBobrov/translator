package org.home.translator.interpreter.brainfuck.command;

import org.home.translator.interpreter.brainfuck.interpreter.BrainfuckProgram;

public class PrintCeilCommand extends AbstractCommand {

    public PrintCeilCommand(BrainfuckProgram program) {
        super(program);
    }

    @Override
    public void execute() {
        program.printCeil();
    }
}
