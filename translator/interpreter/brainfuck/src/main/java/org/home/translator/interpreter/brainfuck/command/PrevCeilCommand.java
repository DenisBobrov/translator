package org.home.translator.interpreter.brainfuck.command;

import org.home.translator.interpreter.brainfuck.interpreter.BrainfuckProgram;

public class PrevCeilCommand extends AbstractCommand {

    public PrevCeilCommand(BrainfuckProgram program) {
        super(program);
    }

    @Override
    public void execute() {
        program.moveToPrevCeil();
    }
}
