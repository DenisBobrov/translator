package org.home.translator.interpreter.brainfuck.command;

import org.home.translator.interpreter.brainfuck.interpreter.BrainfuckProgram;

public class NextCeilCommand extends AbstractCommand {

    public NextCeilCommand(BrainfuckProgram program) {
        super(program);
    }

    @Override
    public void execute() {
        program.moveToNextCeil();
    }
}
